﻿using System;

namespace Tp04Console
{
     public class Program
    {
        public static int air_carre(int cote)
        {
            return cote * cote;
        }
        public static int air_rectangle(int longueur, int largeur)
        {
            return longueur * largeur;
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }
    }
}
